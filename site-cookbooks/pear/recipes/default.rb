#
# Cookbook Name:: pear
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute

%w{php5-dev php-pear make re2c}.each do |pkg|
  package pkg do
    action :install
  end
end

# update the main channels
php_pear_channel 'pear.php.net' do
  action :update
end

php_pear_channel 'pecl.php.net' do
  action :update
end