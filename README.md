## Require

	$ vagrant plugin install vagrant-ami
	$ vagrant plugin install vagrant-aws
	$ vagrant plugin install vagrant-omnibus

## Berks install
	$ bundle install --binstubs
	$ berks install --path cookbooks

## AWS

	$ vagrant up --provider=aws
	$ vagrant create_ami --name my-ami --desc "My AMI" --tags role=test,environment=dev
	$ vagrant destroy
